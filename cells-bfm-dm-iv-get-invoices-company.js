class CellsBfmDmIvGetInvoicesCompany extends Polymer.Element {

  static get is() {
    return 'cells-bfm-dm-iv-get-invoices-company';
  }

  static get properties() {
    return {
      endpoint: String,
      headers: {
        type: String,
        value: () => ({})
      },
      host: {
        type: String,
        value: () => ''
      },
      params:{
        type: Object,
        value: () => ({
          skip:0,
          limit:40
        })
      }
    };
  }
  /**
   * Function to handle service success response
   * @param {*} response
   */
  _successResponse(response) {
    if (response.detail.response.length) {
      setTimeout(function(){
        this._manageDataTransactions(response.detail.response);
      }.bind(this), 15000);
    }
    if (!response.detail.response.length) {

      setTimeout(function () {
        this.$.dpGetTransactions.generateRequest();
      }.bind(this), 3000);
      this.dispatchEvent(new CustomEvent('missing-transactions'));
    }

    this.dispatchEvent(new CustomEvent('all-transactions', {detail: response.detail.response}));
  }
  _manageDataTransactions(transactions){
    var transactionsTable = [];
    this.dispatchEvent(new CustomEvent('hidde-spinner',{detail: true}))
    //this.dispatchEvent(new CustomEvent('transactions-table-data',{detail:transactionsTable}))
    this.dispatchEvent(new CustomEvent('transactions-table-data',{detail:transactions}))
  }
  /**
   * Function to generate enpoint and call service
   */
  starterMethod(data) {
    this._buildEndpoint();
    if (this.endpoint) {
      this.dispatchEvent(new CustomEvent('hidde-spinner',{detail: false}))
      this.$.dpGetCompanyTransactions.generateRequest();
    }
  }
  filter(params){

  }
  /**
   * function to buil enpoint url
   */
  _buildEndpoint(rfc) {
    let host = this._getHost();
    let isMock = this._getMocks();
    this.set('endpoint', `${host}/transactions/${rfc}${isMock}`);
  }
  _getMocks(){
    return (window.AppConfig && window.AppConfig.mocks) ? '.json' : '';
  }
  /**
   * Function to get host from config file
   */
  _getHost() {
    return (window.AppConfig && window.AppConfig.host) ? window.AppConfig.host : this.host;
  }
  /**
   * Function to hanlde error status response
   * @param {*} reponse
   */
  _handlerError(reponse) {
    var msj = 'Estamos teniendo dificultades técnicas, favor de intentar mas tarde';
    try {
      var errorCode = reponse.detail.code;
      switch (errorCode) {
        case 401:
          this.dispatchEvent(new CustomEvent('unauthorized-error', { detail: reponse.detail.code }));
          break;
        default:
          msj = reponse.detail['error-message'] || errorCode || msj;
          break;
      }
    } catch (e) {
      //No aplicable catch handler, use default values from eventError and msj
    }
    this.dispatchEvent(new CustomEvent('hidde-spinner',{detail: true}))
    this.dispatchEvent(new CustomEvent('account-detail', {detail: []}));
    this.dispatchEvent(new CustomEvent('error-service', { detail: msj }));
  }
}

}

customElements.define(CellsBfmDmIvGetInvoicesCompany.is, CellsBfmDmIvGetInvoicesCompany);
